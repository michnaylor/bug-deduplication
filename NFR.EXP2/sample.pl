#!/usr/bin/perl
use strict;
use Heap::Simple;
my $N = (shift @ARGV) || 1;
my $heap = Heap::Simple->new(elements => "Any", max_count => $N);

while(my $line = <>) {
	chomp($line);
	$heap->key_insert(rand(), $line);
}

print join($/, $heap->extract_all() ).$/;
