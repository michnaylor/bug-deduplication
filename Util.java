/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Util {
    public static Set<String> getIntersection(
    		ArrayList<String> documentWords, 
    		ArrayList<String> queryWords) {
        Set<String> documentSet = new HashSet<String>(documentWords);
        Set<String> querySet = new HashSet<String>(queryWords);
        documentSet.remove("");
        documentSet.retainAll(querySet);
        return documentSet;
    }

    public static ArrayList<String> getAllNGrams(Document document, int n) {
	ArrayList<String> documentWords;
	documentWords = get_n_grams(document.getDescription(), n);
	documentWords.addAll(get_n_grams(document.getTitle(), n));
	return documentWords;
    }

    public static ArrayList<String> get_n_grams(String text, int n) {
	String[] splitString = text.split(" ");
	if (n > 1) {
	    return bigramFromString(splitString);
	}
	return new ArrayList<String>(Arrays.asList(splitString));
    }

    private static ArrayList<String> bigramFromString(String[] splitString) {
	ArrayList<String> finalBigram = new ArrayList<String>();
	for (int i = 1; i < splitString.length; i++) {
	    finalBigram.add(splitString[i - 1] + " " + splitString[i]);
	}
	return finalBigram;
    }
}
