/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.ArrayList;

public class TestCleanUp {

    public static void cleanBugs(String project) {
	ElasticSearchManager es = new ElasticSearchManager(project);

	ArrayList<Document> calculated = es.getAlreadyCalculatedBugs();
	System.out.println(calculated.size() + " bugs to clean");
	for (Document doc: calculated) {
	    es.deleteCalcFields(doc);
	}
	/* execute any leftover actions in bulk */
	es.executeBulk();
	es.shutdown();
    }

    public static void main(String[] args) {
	String project = args[0];
        cleanBugs(project);
    }

}