/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.ArrayList;
import java.util.Set;


class Querier {
	private FreeVariables freeVariables;
	private BM25F bm25f;
	private NGramGetter ngramGetter;
	
	private Querier(FreeVariables freeVariables, BM25F bm25f,
				NGramGetter ngramGetter) {
		this.freeVariables = freeVariables;
		this.bm25f = bm25f;
		this.ngramGetter = ngramGetter;
	}

	public double calculateIntersectBM25F(Document document,
							Document query) {
		return calculateBM25F(document,
			Util.getIntersection(
				this.ngramGetter.getNgrams(document),
				this.ngramGetter.getNgrams(query)));

	}

	/*
	 * note: it only really makes sense to call this on a unigram
	 * 		querier.
	 */
	public double calculateIntersectBM25F(Document document,
						ArrayList<String> query) {
		return calculateBM25F(document,
			Util.getIntersection(
				this.ngramGetter.getNgrams(document),
				query));
	}

	public double calculateBM25F(Document document, Set<String> query) {
    		return this.bm25f.calculateBM25F(document, query,
				 this.freeVariables);
	}

	public static Querier makeUnigramQuerier(Corpus corpus, 
			ArrayList<String> importantWords) {

		BM25F bm25f = new BM25F(corpus, 1, false, importantWords);
		return new Querier(FreeVariables.forUnigram(), bm25f,
					new UnigramGetter());
	}

	public static Querier makeBigramQuerier(Corpus corpus,
			ArrayList<String> importantWords) {
		BM25F bm25f = new BM25F(corpus, 2, false, importantWords);
		return new Querier(FreeVariables.forBigram(), bm25f,
					new BigramGetter());
	}


	private interface NGramGetter {
		public ArrayList<String> getNgrams(Document d);
	};

	private static class UnigramGetter implements NGramGetter {
		public ArrayList<String> getNgrams(Document d) {
			return d.getNgram1();
		}
	};

	private static class BigramGetter implements NGramGetter {
		public ArrayList<String> getNgrams(Document d) {
			return d.getNgram2();
		}
	};

	public static class FreeVariables {
		public double[] featureWeights;

		public double wsumm;
		public double wdesc;

		public double bsumm;
		public double bdesc;

		public double k1;
		public double k3;

		private FreeVariables() {
			featureWeights = new double[]{0, 0.9, 0.2, 2.0, 0.0, 0.7, 0.0, 0.0};
			// leading zero as features is 1-indexed
		}

		public static FreeVariables forUnigram() {
			FreeVariables variables = new FreeVariables();
			variables.wsumm = 3.0;
			variables.wdesc = 1.0;
			variables.bsumm = 0.5;
			variables.bdesc = 1.0;
			variables.k1 = 2.0;
			variables.k3 = 0.0;

			return variables;
		}

		public static FreeVariables forBigram() {
			FreeVariables variables = new FreeVariables();
			variables.wsumm =  3.0;
			variables.wdesc =  1.0;
			variables.bsumm =  0.5;
			variables.bdesc =  1.0;
			variables.k1 =  2.0;
			variables.k3 =  0.0;

			return variables;
		}
	}
}
