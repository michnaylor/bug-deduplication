import os, sys, couchdb
sys.path = [os.getcwd()] + sys.path # allow import from databases                                                                                                         

class BugCleaner(object):
    def __init__(self, project):
        self.project = project

    def clean_bugs(self):
        server = couchdb.Server()
        couch = server[self.project]

        count = 0
        for bug in couch.view('_all_docs'):
            # only clean 50 bugs
            if count > 50:
                break
            bug = couch[bug.id]
            if 'bugid' not in bug:
                break
            self.del_fields(bug, 'ngram1')
            self.del_fields(bug, 'ngram2')
            self.del_fields(bug, 'contextMap')
            couch[bug.id] = bug 

    @staticmethod
    def del_fields(bug, field):
        if field in bug:
            del bug[field]


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
            description='delete ngram1, ngram2, and contextMap fields'
        )
    parser.add_argument('project', help='project name')
    args = parser.parse_args()

    BugCleaner(args.project).clean_bugs()
