PROCESSOR=${1:-"android"}
FILE="$PROCESSOR.csv"

export CLASSPATH=./:./NFR.EXP2:./lib/*:/usr/share/java/junit-4.11.jar:.:/usr/share/java/hamcrest-core-1.3.jar:.:./issues:./tests

echo "Calling bug-dedup sample"

java Main --project="$PROCESSOR" --output=/csv_files -m -p
cd ../fastrep

echo "Calling fastrep"

bash runOutNoSort.sh /csv_files/"$FILE"
cd ../bug-deduplication

echo "Cleaning sample calculations"
java TestCleanUp "$PROCESSOR"

echo "Calling bug-dedup no sample"
java Main --project="$PROCESSOR" --output=/csv_files -p