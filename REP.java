/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.ArrayList;
import java.util.Arrays;

public class REP {
    Querier unigramQuerier;
    Querier bigramQuerier;
    Querier.FreeVariables freeVariables;

    public REP(Querier unigramQuerier, Querier bigramQuerier) {
	    this.unigramQuerier = unigramQuerier;
	    this.bigramQuerier = bigramQuerier;

	    this.freeVariables = Querier.FreeVariables.forUnigram();
    }
    
    public double getREP(Document doc, Document query) {
	double[] features = getFeatures(doc, query);
	double result = 0;

	for (int i = 1; i < features.length; i++) {
	    result += freeVariables.featureWeights[i] * features[i];
	}
	return (double) ((int) (result * 1000)) / 1000;
    }

    public double[] getFeatures(Document doc, Document query) {
	double[] features = new double[8];
	features[1] = unigramQuerier.calculateIntersectBM25F(doc, query);
	features[2] = bigramQuerier.calculateIntersectBM25F(doc, query);
	features[3] = 0;
	features[4] = zero_one_function(doc.getComponent(),
					query.getComponent());
	features[5] = zero_one_function(doc.getType(), query.getType());
	features[6] = reciprocal_function(doc.getPriority(),
					  query.getPriority());
	features[7] = calcDiff(versionSplit(doc.getVersion()),
			       versionSplit(query.getVersion()));
	return features;
    }

    public String[] versionSplit(String version) {
	return version.split("[-\\.\\s]");
    }

    public double calcDiff(String[] v1, String[] v2) {
	if (v1.length == 0 && v2.length != 0) {
	    return 1 / (1 + (double) stringToInt(v2[0]));
	}
	else if (v2.length == 0 && v1.length != 0) {
	    return (double) stringToInt(v1[0]);
	}
	else if (v2.length == 0 && v2.length == 0) {
	    return 1;
	}
	else if (v1[0].equals(v2[0])) {
	    return calcDiff(Arrays.copyOfRange(v1, 1, v1.length), 
			    Arrays.copyOfRange(v2, 1, v2.length));
	}

	else {
	    int intV1 = stringToInt(v1[0]);
	    int intV2 = stringToInt(v2[0]);
	    
	    return 1f/(1f + Math.abs(intV1 - intV2));
	}
    }

    public int stringToInt(String v1) {
	try {
	    return Integer.parseInt(v1);
	}
	catch (Exception ex) {
	    return 0;
	}
    }

    private int zero_one_function(Object o1, Object o2) {

	if (o1.equals(o2))
	    return 1;

	return 0;
    }

    private double reciprocal_function(double d1, double d2) {

	return (double) 1 / (1 + Math.abs(d1 - d2));
    }

    public double[] getExtendedFeatures(Document doc, 
					ArrayList<ArrayList<String>> dict) {
	double[] intersectDoc = new double[dict.size()];
	ArrayList<String> documentWords = doc.getNgram1();
	ArrayList<String> queryWords = null;
	for (int i = 0; i < dict.size(); i++) {
	    queryWords = dict.get(i);
	    intersectDoc[i] = unigramQuerier.calculateIntersectBM25F(doc,
			    			queryWords);
	}
	return intersectDoc;
    }

    public double cosineSimilarity(double[] vec1, double[] vec2) {

	double result = 0.0;
        int n = vec1.length;
	for(int i = 0; i < n; i++)
            result += vec1[i] * vec2[i];

        result = result / (magnitude(vec1) * magnitude(vec2));
        result = Math.round(result * 1000) / 1000.0d;

        return result;
    }

    public double magnitude(double[] vector) {

        double magnitude = 0.0;
	for(int i = 0; i < vector.length; i++)
            magnitude += (vector[i] * vector[i]);

	magnitude = Math.sqrt(magnitude);
	return magnitude;
    }
}
