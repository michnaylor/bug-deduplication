/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


import org.lightcouch.CouchDbClient;
import com.google.gson.*;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Arrays;

public class CouchDBManager {
    public static CouchDbClient dbClient;
    private List<Document> notCalculated;

    public CouchDBManager() {
	dbClient = new CouchDbClient("android", false, "http", 
				     "127.0.0.1", 5984, "", "");
    }

    public CouchDBManager(String project) {
	/* Don't create the db if it doesn't exist already */
	Map<String, String> env = System.getenv();
	String host = env.get("COUCHDB_PORT_5984_TCP_ADDR");
	if (host == null)
	    host = "127.0.0.1";
	dbClient = new CouchDbClient(project, false, "http", 
				     host, 5984, null, null);
    }

    public ArrayList<Document> getAllDocs() {
	List<Document> result = dbClient.view("_all_docs")
	    .includeDocs(true)
	    .query(Document.class);
	return new ArrayList(result);
    }

    public ArrayList<Document> getAlreadyCalculatedBugs() {
	return new ArrayList(getTempViewDocs(true));
    }

    public ArrayList<Document> getNotCalculatedBugs() {
	notCalculated = getTempViewDocs(false);
	return new ArrayList<Document>(Arrays.asList(deepCopy(notCalculated)));
    }

    private List<Document> getTempViewDocs(Boolean alreadyCalculated) {
	String tempView = "noCalculated";
	if (alreadyCalculated) {
	    tempView = "yesCalculated";
	}
	List<Document> allDocs = dbClient.view("_temp_view")
	    .tempView(tempView)
	    .includeDocs(true)
	    .query(Document.class);
	return allDocs;
    }

    private Document[] deepCopy(List<Document> original) {
	Gson gson = new Gson();
	String json = gson.toJson(original);

	return gson.fromJson(json, Document[].class);
    }

    public void bulkUpdateDocs(Map<String, Document> updatedDict) {
	for (Document doc: notCalculated) {
	    Document updated = updatedDict.get(doc.getBugID());
	    if (updated != null) {
		doc.setNgram1(updated.getNgram1());
		doc.setNgram2(updated.getNgram2());
		doc.setContextMap(updated.getContextMap());
	    }
	}
	updatedDict = null;
	dbClient.bulk(notCalculated, true);
    }

    public void shutDownCouchdb() {
	dbClient.shutdown();
    }
}