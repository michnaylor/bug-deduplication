/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.*;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

public class REPDumper {
    protected REP rep;
    private Boolean stemming = false;
    private String FILENAME = "out.csv";
    protected Dictionaries allDictionaries;
    public final Map<String, Document> allDocs = 
	new HashMap<String, Document>();
    public BufferedWriter output = null;
    private String HEADER = "id1,id2,class,REP,cosine_architecture,"
	+ "cosine_nfr,cosine_junk,cosine_lda,cosine_user";

    public REPDumper(Dictionaries dicts, Boolean stemming) {
	this.stemming = stemming;
	this.allDictionaries = dicts;
    }

    public REPDumper(Dictionaries dicts, Boolean stemming, String filename) {
	this.stemming = stemming;
	this.allDictionaries = dicts;
	this.FILENAME = filename;
    }

    public static void warn(String warning) {
	System.err.println("WARNING: " + warning);
    }

    public ArrayList<Document> processDocs(ArrayList<Document> docsToProcess) {
	Preprocessing prep = new Preprocessing();
	docsToProcess = prep.process(docsToProcess, stemming);
	Collections.sort(docsToProcess);

	return docsToProcess;
    }

    public ArrayList<Document> processDocsDupeStrategy(ArrayList<Document> 
						       docsToProcess) {
	Preprocessing prep = new Preprocessing(Preprocessing.DupeStrategy
					       .IGNORE_DUPES);
	docsToProcess = prep.process(docsToProcess, stemming);
	Collections.sort(docsToProcess);

	return docsToProcess;	
    }

    public int getNumDups(ArrayList<Document> docs) {
	int counter = 0;
	for (Document doc1: docs) {
	    if (doc1.getStatus().equals("duplicate"))
		counter++;
	}
	System.err.println("number of docs, post processings: " + docs.size());
	System.err.println("number of duplicates: " + counter);
	return counter;
    }

    public void initCorpusAndREP(ArrayList<Document> docs) {
	Corpus inputCorpus = new Corpus(docs);
	Querier unigram = Querier.makeUnigramQuerier(inputCorpus, 
				  allDictionaries.getImportantWords());
	Querier bigram = Querier.makeBigramQuerier(inputCorpus, 
				  allDictionaries.getImportantWords());
	rep = new REP(unigram, bigram);
    }

    public Boolean doIndividualDocCalcs(Document doc) {
	Document nGramDoc = allDocs.get(doc.getBugID());
	if (nGramDoc != null)
	    return false;
	doc.setNgram1(Util.getAllNGrams(doc, 1));
	doc.setNgram2(Util.getAllNGrams(doc, 2));
	doc.setContextMap(getContextMap(doc));
	allDocs.put(doc.getBugID(), doc);
	return true;
    }

    public Map<String, double[]> getContextMap(Document doc) {
	final double[] NFR = rep.getExtendedFeatures(doc,
						     allDictionaries.getNFR());
	final double[] junk = rep.getExtendedFeatures(doc,
						      allDictionaries.getJunk());
	final double[] arch = rep.getExtendedFeatures(doc,
						      allDictionaries.getArch());
	final double[] LDA = rep.getExtendedFeatures(doc,
						     allDictionaries.getLDA());
	final double[] user = rep.getExtendedFeatures(doc,
						      allDictionaries.getUser());
	Map<String, double[]> contextMap = new HashMap<String, double[]>() {
	    {
		put("NFR", NFR);
		put("junk", junk);
		put("arch", arch);
		put("LDA", LDA);
		put("user", user);
	    }};
	return contextMap;
    }

    public String dump(Document doc1, Document doc2) {
	String id1 = doc1.getBugID();
        String id2 = doc2.getBugID();
	String masterId1 = doc1.getMergeID();
	String masterId2 = doc2.getMergeID();
        
        CosineResults cosineResults = new CosineResults(doc1, doc2);

	boolean isDuplicate = (masterId1.equals("0") && 
                               masterId2.equals("0")) ? false
	    : ((masterId1.equals(masterId2)
		|| masterId1.equals(id2)
		|| id1.equals(masterId2)));
	StringBuffer line = new StringBuffer("");
	String separator = ",";
	line.append(id1 + separator);
	line.append(id2 + separator);
	line.append(((isDuplicate) ? "dup" + separator : "non" + separator));
	line.append(cosineResults.REP + separator);
	line.append(cosineResults.cosine_architecture + separator);
	line.append(cosineResults.cosine_nfr + separator);
	line.append(cosineResults.cosine_junk + separator);
	line.append(cosineResults.cosine_lda + separator);
	line.append(cosineResults.cosine_user);

	return line.toString();
    }

    public Map<String, Double> dumpToMap(Document doc1, Document doc2) {
        CosineResults cosineResults = new CosineResults(doc1, doc2);
        return cosineResults.toMap();
    }

    public void initFileWriter(Boolean newFile) {
	File file = new File(FILENAME);
	Boolean fileExists = file.exists();
	try {
	    if (newFile) {
		file.delete();
		file.createNewFile();
	    }
	    /* create new filewriter that appends */
	    FileWriter fw = new FileWriter(file, true);
	    output = new BufferedWriter(fw);
	}
	catch (IOException e) {
	    e.printStackTrace();
	}
	if (!fileExists) {
	    writeToFile(HEADER);
	}
    }

    public void writeToFile(String line) {
	try {
	    output.write(line + "\n");
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    public void cleanUpObjects() {
	rep = null;
	allDictionaries = null;
    }

    public void closeWriter() {
	try {
	    output.flush();
	    output.close();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    } 

    private class CosineResults {
	double REP;
	double cosine_architecture;
	double cosine_nfr;
	double cosine_junk;
	double cosine_lda;
	double cosine_user;

        private CosineResults(Document doc1, Document doc2) {
            Map<String, double[]> contextMap1 = doc1.getContextMap();
            Map<String, double[]> contextMap2 = doc2.getContextMap();
            
            REP = rep.getREP(doc1, doc2);
            cosine_architecture = rep.cosineSimilarity(contextMap1.get("arch"),
                                                       contextMap2.get("arch"));
            cosine_nfr = rep.cosineSimilarity(contextMap1.get("NFR"),
                                              contextMap2.get("NFR"));
            cosine_junk = rep.cosineSimilarity(contextMap1.get("junk"),
                                               contextMap2.get("junk"));
            cosine_lda = rep.cosineSimilarity(contextMap1.get("LDA"),
                                              contextMap2.get("LDA"));
            cosine_user = rep.cosineSimilarity(contextMap1.get("user"),
                                               contextMap2.get("user"));
        }

        private Map<String, Double> toMap() {
            Map<String, Double> cosineResults = new HashMap<String, Double>();
            cosineResults.put("REP", new Double(REP));
            cosineResults.put("cosine_architecture", 
                              new Double(cosine_architecture));
            cosineResults.put("cosine_nfr", new Double(cosine_nfr));
            cosineResults.put("cosine_junk", new Double(cosine_junk));
            cosineResults.put("cosine_lda", new Double(cosine_lda));
            cosineResults.put("cosine_user", new Double(cosine_user));

            return cosineResults;
        }
    }
}
