/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class IncrementalProcessor extends Processor {
    private ArrayList<Document> calculated;
    private ArrayList<Document> notCalculated;
    private static CouchDBManager couchdb;
    private final ByteArrayOutputStream errContent = 
	new ByteArrayOutputStream();

    public IncrementalProcessor() {
	couchdb = new CouchDBManager("android");
    }

    public IncrementalProcessor(REPDumper rd) {
	super(rd);
	couchdb = new CouchDBManager("android");
	rd.initFileWriter(false);
    }

    public IncrementalProcessor(REPDumper rd, String projectName, 
                                String directory) {
	super(rd);
	couchdb = new CouchDBManager(projectName);
	rd.initFileWriter(false);
    }

    public void setCouchDBManager(String dbName) {
	couchdb = new CouchDBManager(dbName);
    }

    public void process(Boolean parallel) {
	calculated = couchdb.getAlreadyCalculatedBugs();
	/* TODO: Does calculated need to be processed? */
	calculated = rd.processDocsDupeStrategy(calculated);
        ArrayList<Document> unprocessed = couchdb.getNotCalculatedBugs();
	notCalculated = rd.processDocsDupeStrategy(unprocessed);

	int dupesCount = rd.getNumDups(calculated) 
	    + rd.getNumDups(notCalculated);
	/* TODO: Should corpus be all docs, or just not calculated? */
	ArrayList<Document> allDocs = new ArrayList<Document>();
	allDocs.addAll(calculated);
	allDocs.addAll(notCalculated);
	rd.initCorpusAndREP(allDocs);
	allDocs = null;

	if (parallel)
	    parallelProcess();
	else
	    nonParallelProcess();
	
	calculated = null;
	notCalculated = null;
	rd.cleanUpObjects();
	couchdb.bulkUpdateDocs(rd.allDocs);
	couchdb.shutDownCouchdb();
	rd.closeWriter();
    }
    
    public void nonParallelProcess() {
	/* perform calculations for new docs */
	for (Document doc: notCalculated) {
	    rd.doIndividualDocCalcs(doc);
	}

	/* old vs new */
	int count = 0;
	for (Document doc1 : calculated) {
	    for (Document doc2 : notCalculated) {
		String dump = rd.dump(doc1, doc2);
		rd.writeToFile(dump);
		count++;
	    }
	}

	/* new vs new */
	for (int i = 0; i < notCalculated.size() - 1; i++) {
	    for (int j = i + 1; j < notCalculated.size(); j++) {
		String dump = rd.dump(notCalculated.get(i),
				      notCalculated.get(j));
		rd.writeToFile(dump);
		count++;
	    }
	}

	System.out.println(count + " lines written.");
    }

    public void parallelProcess() {
	/* perform calculations for new docs */
	for (Document doc: notCalculated) {
	    rd.doIndividualDocCalcs(doc);
	}

	/* setup final variables */
	final ArrayList<Document> fcalculated = calculated;
	final ArrayList<Document> fnotCalculated = notCalculated;

	final LinkedBlockingQueue<ArrayList<String>> queue = 
	    new LinkedBlockingQueue<ArrayList<String>>();
	ExecutorService executor = getExecutor();
	int threadCount = 0;

	for (int i = 0; i < fnotCalculated.size(); i++) {
	    final Document fdoc1 = fnotCalculated.get(i);
	    final int di = i;
	    Thread t = new Thread(new Runnable() {
		    public void run() {
			ArrayList<String> results = new ArrayList<String>();
			String dump = "";
			for (Document doc2: fcalculated) {
			    /* new vs old */
			    dump = rd.dump(fdoc1, doc2);
			    results.add(dump);
			}
			if (di == fnotCalculated.size() - 1) {
			    addResultsToQueue(results);
			    return;
			}
			for (int j = di + 1; j < fnotCalculated.size(); j++) {
			    /* new vs new */
			    Document doc3 = fnotCalculated.get(j);
			    dump = rd.dump(fdoc1, doc3);
			    results.add(dump);
			}
			addResultsToQueue(results);
		    }

		    public void addResultsToQueue(ArrayList<String> results) {
			boolean done = false;
			while (!done) {
			    try {
				queue.put(results);
				done = true;
			    } catch (InterruptedException e) {
				System.err.println(e.toString());
			    }
			}
		    }
		});
	    
	    executor.execute(t);
	    threadCount++;
	}

	// blocking take
	// once threadCount is 0 we're done!
	int lineCount = 0;
	try {
	    while (threadCount > 0) {
		ArrayList<String> res = queue.take();
		for (String line : res) {
		    rd.writeToFile(line);
		    lineCount++;
		}
		threadCount--;
	    }
	} catch (InterruptedException e) {
	    throw new Error(e.toString());
	}
	executor.shutdown();
        while (!executor.isTerminated()) {
	}

	System.out.println(lineCount + " lines written.");
    }
}