/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.ArrayList;
import java.util.concurrent.*;

public class Processor {
    public REPDumper rd;
    public ArrayList<Document> docs;

    public Processor(REPDumper rd) {
	this.rd = rd;
    }

    public Processor() {

    }

    public void setREPDumper(REPDumper rd) {
	this.rd = rd;
    }

    public ExecutorService getExecutor() {
	Runtime runtime = Runtime.getRuntime();
	int nrOfProcessors = runtime.availableProcessors();
	return Executors
	    .newFixedThreadPool(Math.max(1, nrOfProcessors - 1));
    }

    public void process(Boolean parallel) {
	throw new UnsupportedOperationException("The method 'process'"
						+ " is not implemented "
						+"for this class.");
    }

    public void nonParallelProcess() {
	throw new UnsupportedOperationException("The method 'nonParallelprocess'"
						+ " is not implemented "
						+"for this class.");

    }

    public void parallelProcess() {
	throw new UnsupportedOperationException("The method 'parallelprocess'"
						+ " is not implemented "
						+"for this class.");

    }
}