import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

public class TestBugSampler {
    private static ElasticSearchManager es;
    private static Document doc;
    private static Document doc1;
    private static ArrayList<Document> allDocs = new ArrayList<Document>();

    @BeforeClass
    public static void setup() {
        TestCleanUp tc = new TestCleanUp();
        tc.cleanBugs("mini_android");

        es = new ElasticSearchManager("mini_android");
    }

    @Test
    public void testGetRandomDups() {
        ArrayList<Document> results = new BugSampler(es, 4).getDupeSample();
        assertEquals(2, results.size());
		// there is one dupe in mini_android, so we expect to get it
		// and its parent

	HashSet<String> receivedIDs = new HashSet<String>();
        for (Document doc : results) {
		receivedIDs.add(doc.getBugID());
	}
	assert receivedIDs.contains("100"); // the parent
	assert receivedIDs.contains("10004"); // the child
    }

    @Test
    public void testGetRandomNonDups() {
        ArrayList<Document> results = new BugSampler(es, 10).getNonDupeSample();
        assertEquals(results.size(), 8);

        ArrayList<Document> results2 = new BugSampler(es, 10).getNonDupeSample();
        
        Boolean equals = true;
        for (int i = 0; i < results2.size(); i++) {
            if (!results.get(i).getBugID().equals(results2.get(i).getBugID())) {
                equals = false;
            }
        }
        assertFalse(equals);
    }
}

