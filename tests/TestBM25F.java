import java.util.*;
import org.junit.Test;
import org.junit.BeforeClass;
import static org.junit.Assert.assertEquals;

public class TestBM25F {
    private static BM25F bm25f;
    private static ArrayList<Document> docs;
    
    @BeforeClass
    public static void setup() {
        CouchDBManager couch = new CouchDBManager("mini_android");
	docs = couch.getAllDocs();
	bm25f = new BM25F(new Corpus(docs), 1, false, new ArrayList<String>());
    }
    
    @Test
    public void testCalculateBM25F() {
	REP rep = new REP(null, null);
	Document document = docs.get(1);
	Document query = docs.get(2);
	ArrayList<String> documentWords = Util.getAllNGrams(document, 1);
	ArrayList<String> queryWords = Util.getAllNGrams(query, 1);

	Set<String> intersection = Util.getIntersection(documentWords, 
						       queryWords);

	double result = bm25f.calculateBM25F(document, intersection,
					     rep.freeVariables);
	assertEquals(result, 0.27523, 0.001);
    }

    @Test
    public void testCountFrequencies() {
	String str = "this sentence  should have have two matches";
	int count = bm25f.countFrequencies(str, "have");
	assertEquals(2, count);

	count = bm25f.countFrequencies(str, "have have");
	assertEquals(1, count);

	str = "this word has no matches";
	count = bm25f.countFrequencies(str, "nomatcheshere");
	assertEquals(0, count);
    }
}
