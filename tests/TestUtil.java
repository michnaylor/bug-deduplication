import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestUtil {
    private Util util = new Util();

    @Test
    public void testUnigram() {
	String text = "this sentence has five words";
	ArrayList<String> unigram = util.get_n_grams(text, 1);
	assertEquals(text.split(" ")[0], unigram.get(0));

	text = "thissentencehasoneword";
	unigram = util.get_n_grams(text, 1);
	assertEquals(text.split(" ")[0], unigram.get(0));
	    
    }

    @Test
    public void testBigram() {
	String text = "this sentence has five words";
	ArrayList<String> bigram = util.get_n_grams(text, 2);
	ArrayList<String> bigramFromSplit = bigramFromString(text);
	assertEquals(bigramFromSplit.size(), bigram.size());
    }

    public ArrayList<String> bigramFromString(String text) {
	String[] splitText = text.split(" ");
	ArrayList<String> finalBigram = new ArrayList<String>();
	for (int i = 1; i < splitText.length; i++) {
	    finalBigram.add(splitText[i - 1] + " " + splitText[i]);
	}
	return finalBigram;
    }
}