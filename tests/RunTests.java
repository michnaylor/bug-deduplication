import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class RunTests {
    public static void runTest(Class testClass) {
	System.out.println("---------------------------------------");
	System.out.println("Running tests for " + testClass.getName());
	Result result = JUnitCore.runClasses(testClass);
	for (Failure failure : result.getFailures()) {
	    System.out.println("FAILURE");
	    System.out.println(failure.toString());
	}
	int numPassed = result.getRunCount() - 
	    result.getFailures().size();
	System.out.println(testClass.getName() + ": " + numPassed  
			   + " tests were successful.");
    }

    public static void main(String[] args) {
	runTest(TestREPDumper.class);
	runTest(TestBM25F.class);
	runTest(TestCouchDBManager.class);
	runTest(TestElasticSearchManager.class);
	runTest(TestIncrementalProcessor.class);
        runTest(TestLogit.class);
	runTest(TestREP.class);
	runTest(TestCorpus.class);
	runTest(TestPreprocessing.class);
	runTest(TestUtil.class);
    }
}
