import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import org.junit.Before;

public class TestREPDumper {
    private REPDumper rd = null;
    private double[] NFR = {0.0, 0.0, 0.0, 0.0, 0.0, 0.6684077258016061};

    @Before
    public void setup() {
        Dictionaries allDictionaries = new Dictionaries("mini_android", "",
                                                        false, "");
        rd = new REPDumper(allDictionaries, false);
    }

    @Test
    public void testGetContextMapCouch() {
        CouchDBManager couch = new CouchDBManager("mini_android");
        ArrayList<Document> notCalculated = couch.getNotCalculatedBugs();
        notCalculated = rd.processDocs(notCalculated);
	rd.initCorpusAndREP(notCalculated);
        Document doc1 = notCalculated.get(2);

        Map<String, double[]> contextMap = performCalc(doc1);
        assertTrue(Arrays.equals(contextMap.get("NFR"), NFR));
    }

    @Test
    public void testGetContextMapES() {
        ElasticSearchManager es = new ElasticSearchManager("mini_android");
        ArrayList<Document> notCalculated = es.getNotCalculatedBugs();
        notCalculated = rd.processDocs(notCalculated);
	rd.initCorpusAndREP(notCalculated);
        Document doc1 = notCalculated.get(2);

        Map<String, double[]> contextMap = performCalc(doc1);
        assertTrue(Arrays.equals(contextMap.get("NFR"), NFR));
    }

    public Map<String, double[]> performCalc(Document doc1) {
        rd.doIndividualDocCalcs(doc1);
        return doc1.getContextMap();
    }
}