import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.BufferedWriter;
import java.lang.Thread;
import java.io.IOException;
import java.util.Map;
import java.util.PriorityQueue;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.After;

public class TestElasticProcessor {
    private REPDumper rd = null;
    private ElasticProcessor proc = null;
    private final ByteArrayOutputStream outContent = 
	new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = 
	new ByteArrayOutputStream();
    private PrintStream stdout;

    @Before
    public void setUpStreams() {
        cleanBugs();

	stdout = System.out;
	System.setOut(new PrintStream(outContent));
	System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
	System.setOut(stdout);
	System.setErr(stdout);
	proc = null;

        cleanBugs();
    }

    private void cleanBugs() {
        Thread t = new Thread(new Runnable() {
                public void run() {
                    TestCleanUp tc = new TestCleanUp();
                    tc.cleanBugs("mini_android");
                }
            });

        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void smokeNonParallel() {
	proc = new ElasticProcessor(getREPDumper(),
                                    "mini_android", "");
	proc.rd.output = new BufferedWriter(new OutputStreamWriter(System.out));
        proc.process(false);
    }

    @Test
    public void smokeParallel() {
	proc = new ElasticProcessor(getREPDumper(),
                                    "mini_android", "");
	proc.rd.output = new BufferedWriter(new OutputStreamWriter(System.out));
        proc.process(true);
    }

    @Test
    public void smokeNonParallelHeaps() {
	proc = new ElasticProcessor(getREPDumper(),
                                    "mini_android", "", false);
        proc.process(false);
    }

    @Test
    public void smokeParallelHeaps() {
	proc = new ElasticProcessor(getREPDumper(),
                                    "mini_android", "", false);
        proc.process(true);
    }


    private REPDumper getREPDumper() {
	Dictionaries dict = new Dictionaries("", "", false, "");
	rd = new REPDumper(dict, false);
	return rd;
    }

}