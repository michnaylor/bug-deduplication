import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;


public class TestREP {
    public static REP rep = null;
    public static ArrayList<Document> docs;
    public static REPDumper rd;

    @BeforeClass
    public static void setup() {
        CouchDBManager couch = new CouchDBManager("mini_android");
	docs = couch.getAllDocs();

        Dictionaries dict = new Dictionaries("", "",
                                             false, "");
        rd = new REPDumper(dict, false);
	rd.initCorpusAndREP(docs);
	Corpus corpus = new Corpus(docs);
	Querier unigram = Querier.makeUnigramQuerier(corpus,
					new ArrayList<String>());
	Querier bigram = Querier.makeBigramQuerier(corpus,
					new ArrayList<String>());
	rep = new REP(unigram, bigram);
    }

    @AfterClass
    public static void teardown() {
    }

    @Test
    public void testMagnitude() {
	double[] v1 = {1, 2, 3};
	double[] v2 = {46.2, 71.22, 1.34};
	double[] v3 = {0, 1, 2};

	double mag = rep.magnitude(v1);
	assertEquals(3.741, mag, 0.002);
	
	mag = rep.magnitude(v2);
	assertEquals(84.903, mag, 0.002);
    }

    @Test
    public void testCosineSimilarity() {
	double[] v1 = {1, 2, 3};
	double[] v2 = {46.2, 71.22, 1.34};
	double[] v3 = {0, 0, 0};
	double cosSim = rep.cosineSimilarity(v1, v2);
	assertEquals(0.606, cosSim, 0.002);

	cosSim = rep.cosineSimilarity(v1, v1);
	assertEquals(1, cosSim, 0.002);

	cosSim = rep.cosineSimilarity(v1, v3);
	assertEquals(0, cosSim, 0.002);
    }

    @Test
    public void testGetIntersection() {
	String[] string1 = "abcdefg".split("");
	String[] string2 = "fghijkl".split("");
	String[] string3 = "mnopqrs".split("");
	ArrayList<String> vstring1 = new ArrayList<String>(Arrays.asList(string1));
	ArrayList<String> vstring2 = new ArrayList<String>(Arrays.asList(string2));
	ArrayList<String> vstring3 = new ArrayList<String>(Arrays.asList(string3));

	Set<String> result = Util.getIntersection(vstring1, vstring2);
	assertEquals(2, result.size());

	result = Util.getIntersection(vstring1, vstring3);
	assertEquals(0, result.size());
    }

    @Test
    public void testGetREP() {
	Document doc1 = docs.get(0);
	Document doc2 = docs.get(1);
	rd.doIndividualDocCalcs(doc1);
	rd.doIndividualDocCalcs(doc2);
	double result = rep.getREP(doc1, doc2);
	/* Note: REP will be different here than in TestREPDumper
	 * because these bugs have not gone through preprocessing */
	assertEquals(result, 0.835, 0.001);
    }

    @Test
    public void testGetExtendedFeatures() {
	ArrayList<ArrayList<String>> lda = rd.allDictionaries.getJunk();
	Document doc = docs.get(1);
	rd.doIndividualDocCalcs(doc);
	double[] result = rep.getExtendedFeatures(doc, lda);

	assertEquals(result[10], 1.6565, 0.001);
    }
}
