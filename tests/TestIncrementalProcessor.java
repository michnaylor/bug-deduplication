import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.BufferedWriter;
import java.lang.Process;
import java.io.IOException;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.After;

public class TestIncrementalProcessor {
    private REPDumper rd = null;
    private Parser parser = null;
    private IncrementalProcessor proc = null;
    private final ByteArrayOutputStream outContent = 
	new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = 
	new ByteArrayOutputStream();
    private PrintStream stdout;

    @Before
    public void setUpStreams() {
	stdout = System.out;
	System.setOut(new PrintStream(outContent));
	System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
	System.setOut(stdout);
	System.setErr(stdout);
	try {
	    Process p = Runtime.getRuntime()
		.exec("python util/cleanup_after_tests.py mini_android");
	    p.waitFor();
	} catch (IOException e) {
	    e.printStackTrace();
	} catch (InterruptedException ex) {
	    ex.printStackTrace();
	}
	proc = null;
    }
    
    @Test
    public void testNonParallel() {
	proc = new IncrementalProcessor(getREPDumper("CouchDBParser"),
					"mini_android", "");
	proc.rd.output = new BufferedWriter(new OutputStreamWriter(System.out));
	
	proc.process(false);
	
	String result1 = "101,1001,non,1.248,0.0,0.0,0.0,0.0,0.0";
	String result2 = "101,10007,non,1.414,0.0,0.347,0.0,0.0,0.0";
	String result4 = "8385 lines written";
	assertTrue(outContent.toString().contains(result4));
	assertTrue(outContent.toString().contains(result1));
	assertTrue(outContent.toString().contains(result2));
    }

    @Test
    public void testParallel() {
	proc = new IncrementalProcessor(getREPDumper("CouchDBParser"),
					"mini_android", "");
	proc.rd.output = new BufferedWriter(new OutputStreamWriter(System.out));
	
	proc.process(true);
	String result1 = "101,1001,non,1.248,0.0,0.0,0.0,0.0,0.0";
	String result2 = "101,10007,non,1.414,0.0,0.347,0.0,0.0,0.0";
	String result3 = "8385 lines written";
	String output = outContent.toString();

	assertTrue(output.contains(result1));
	assertTrue(output.contains(result2));
	assertTrue(output.contains(result3));
    }


    private REPDumper getREPDumper(String parserName) {
	Dictionaries dict = new Dictionaries(parserName, "", false, "");
	rd = new REPDumper(dict, false);
	return rd;
    }
}